
(sde:clear)


#we seta few variables for later use

(define W 200)
(define L 1)

(define Plow 5e12)
(define Nca 1e19)
(define Pan 1e19)

# We define a simulation domain and contacts and assign them to the opposite boundaries of the domain

(sdegeo:create-rectangle (position 0 0 0) (position L W 0) "Silicon" "RSub")

(sdegeo:define-contact-set "anode" 4  (color:rgb 1 0 0 ) "##")
(sdegeo:define-contact-set "cathode" 4  (color:rgb 1 0 0 ) "##")

(sdegeo:set-current-contact-set "anode")
(sdegeo:set-contact-edges (find-edge-id (position (* 0.5 L) W 0)))

(sdegeo:set-current-contact-set "cathode")
(sdegeo:set-contact-edges (find-edge-id (position (* 0.5 L) 0 0)))




#We define the doping profiles using gaussian analytical profiles

(sdedr:define-refeval-window "RefEval_anode" "Line"  (position -10 W 0) (position (+ L 10) W 0))

(sdedr:define-gaussian-profile "DopAnode" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" Pan "ValueAtDepth" Plow "Depth" 5 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "Place_anode" "DopAnode" "RefEval_anode" "Both" "NoReplace" "Eval")


(sdedr:define-refeval-window "RefEval_cathode" "Line"  (position -10 0 0) (position (+ L 10) 0 0))

(sdedr:define-gaussian-profile "DopCathode" "PhosphorusActiveConcentration" "PeakPos" 0  "PeakVal" Nca "ValueAtDepth" Plow "Depth" 5 "Gauss"  "Factor" 0.8)
(sdedr:define-analytical-profile-placement "Place_cathode" "DopCathode" "RefEval_cathode" "Both" "NoReplace" "Eval")


# We define substrate doping (p or n, what resistivity)

(sdedr:define-constant-profile "ConstantProfileDefinition_1" "BoronActiveConcentration" Plow)
(sdedr:define-constant-profile-material "ConstantProfilePlacement_1" "ConstantProfileDefinition_1" "Silicon")

# Define meshing rules for refinement of mesh following doping profiles

(sdedr:define-refinement-size "RefinementDefinition_1" (* 0.3 L) (/ W 20.0) 1 0.01 0.01 0.01 )
(sdedr:define-refinement-placement "RefinementPlacement_1" "RefinementDefinition_1" (list "material" "Silicon" ) )
(sdedr:define-refinement-function "RefinementDefinition_1" "DopingConcentration" "MaxTransDiff" 1)

(sde:build-mesh "snmesh" " " "n@node@_msh")







