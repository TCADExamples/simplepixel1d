#setdep @node|DIODE1D@


device DIODE {
File {
    Grid       = "n@node|DIODE1D@_msh.tdr"
    Current    = "@plot@"
    Plot       = "@tdrdat@"
    ##parameter = "@parameter@"
}



Electrode {
	{name = "cathode"   voltage = 0.0    	eRecVelocity=1e7 hRecVelocity=1e7 }	
	{name = "anode"   voltage = 0.0    	eRecVelocity=1e7 hRecVelocity=1e7 }	
		
}


Physics { 
	##AreaFactor=1e8
	##Fermi
 }
        
 Physics (material="Silicon") { 

 	Mobility( 
		DopingDep(Unibo)
            	HighFieldsat(GradQuasiFermi)
 	  	)
  	Recombination( 
		 SRH(DopingDep TempDep)
		 Auger
  		##hAvalanche(UniBo) eAvalanche(UniBo)  		
  		hAvalanche eAvalanche 		
		)
  	EffectiveIntrinsicDensity(OldSlotboom)
  	
  	## defect density = fluence x eta 

## state 1: acceptor, E = Ec-0.42 eV, sigma_e = 2e-15 cm2, sigma_h=2e-14 cm2, eta = 1.613 cm-1
## state 2: acceptor, E = Ec-0.46 eV, sigma_e = 5e-15 cm2, sigma_h=5e-14 cm2, eta = 100 cm-1
## state 3: donor, E = Ev+0.36 eV, sigma_e = 2.5e-14 cm2, sigma_h =2.5e-15 cm2, eta = 0.9 cm-1

 	Traps(
  			(
  			name="state1" acceptor conc=@<fluence*1.613>@
  			Level FromConductionBand	EnergyMid=0.42
  			eXsection=2E-15  hXsection=2E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state2" acceptor conc=@<fluence*100.0>@
  			Level FromConductionBand	EnergyMid=0.46
  			eXsection=5E-15  hXsection=5E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state3" donor conc=@<fluence*0.9>@
  			Level FromValenceBand	EnergyMid=0.36
  			eXsection=2.5E-14  hXsection=2.5E-15
			##eJfactor=1.0 hJfactor=1.0
			)
		)

  	

} 

CurrentPlot {
eLifeTime(Maximum(material="Silicon"))
hLifeTime(Maximum(material="Silicon"))
eAvalanche(Maximum(material="Silicon"))
hAvalanche(Maximum(material="Silicon"))
}
}


Math {

	##CDensityMin=1e-100
	Extrapolate
    Derivatives
  Avalderivatives
  Digits=7
  Notdamped=1000
  Iterations=15
  RelerrControl
  ErrRef(electron)=1e6
  ErrRef(hole)=1e6
  RhsMin=1e-15
  
    eMobilityAveraging=ElementEdge       
  hMobilityAveraging=ElementEdge       
  ParallelToInterfaceInBoundaryLayer(-ExternalBoundary)

}

system {
	DIODE D1 ("anode"=a "cathode"=c) {}
	Vsource_pset va (a gnd) {dc=0}
	Vsource_pset vc (c gnd) {dc=0}
	
	set(gnd=0)
}


Solve {

   Poisson
  Coupled { Poisson Electron Hole }

              	
   Quasistationary (  		DoZero
   			MaxStep=0.02  MinStep=5e-6 InitialStep=1e-4
			Increment=1.6 Decrement=4.0
                  		Goal { Parameter="vc"."dc" Voltage=-0.6 } 
                 		)
                  { Coupled {  Poisson Electron Hole } } 
      
 NewCurrent="CV_"    

    Quasistationary (  		DoZero
   			MaxStep=0.02  MinStep=5e-6 InitialStep=1e-4
			Increment=1.6 Decrement=4.0
                  		Goal { Parameter="vc"."dc" Voltage=100 } 
                 		)
                  { 
                  	  ACCoupled (
                  	  	StartFrequency=1.0 EndFrequency=1.0
                  	  	NumberOfPoints=1 Decade
                  	  	Node(a c) Exclude(va vc)
                  	  	) {  Poisson Electron Hole } 
                  } 


}
   
file {
	ACExtract = "@acplot@"
    output = "@log@"
}

Plot {
	
        Current/Vector	
	eCurrent/Vector
	hCurrent/Vector
	eDensity
	hDensity
	ElectricField/Vector
	Potential
	CurrentPotential
  	DopingConcentration	
	eMobility
	hMobility
	DonorConcentration
	AcceptorConcentration
 	AvalancheGeneration
 	
 	eAvalanche hAvalanche
 	eLifeTime hLifeTime


}

