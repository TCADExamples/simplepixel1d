#setdep @node|DIODE1D@


File {
    Grid       = "n@node|DIODE1D@_msh.tdr"
    Current    = "@plot@"
    Plot       = "@tdrdat@"
    output = "@log@"
    parameter = "@parameter@"
}



Electrode {
	{name = "cathode"   voltage = 0.0    	eRecVelocity=1e7 hRecVelocity=1e7 }	
	###{name = "anode"   voltage = 0.0    voltage=((-1000 0)(-100 @<-HV>@) (0 @<-HV>@))	eRecVelocity=1e7 hRecVelocity=1e7 }	
	{name = "anode"   voltage = 0.0    	eRecVelocity=1e7 hRecVelocity=1e7 }	
		
}


Physics { 
	##AreaFactor=1e8
	##Fermi
 }
        
 Physics (material="Silicon") { 

 	Mobility( 
		DopingDep(Unibo)
            	HighFieldsat(GradQuasiFermi)
 	  	)
  	Recombination( 
		 SRH(DopingDep TempDep)
		 Auger
  		##hAvalanche(UniBo) eAvalanche(UniBo)  		
  		hAvalanche eAvalanche 		
		)
  	EffectiveIntrinsicDensity(OldSlotboom)
 
## defect density = fluence x eta 

## state 1: acceptor, E = Ec-0.42 eV, sigma_e = 2e-15 cm2, sigma_h=2e-14 cm2, eta = 1.613 cm-1
## state 2: acceptor, E = Ec-0.46 eV, sigma_e = 5e-15 cm2, sigma_h=5e-14 cm2, eta = 100 cm-1
## state 3: donor, E = Ev+0.36 eV, sigma_e = 2.5e-14 cm2, sigma_h =2.5e-15 cm2, eta = 0.9 cm-1

 	Traps(
  			(
  			name="state1" acceptor conc=@<fluence*1.613>@
  			Level FromConductionBand	EnergyMid=0.42
  			eXsection=2E-15  hXsection=2E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state2" acceptor conc=@<fluence*100.0>@
  			Level FromConductionBand	EnergyMid=0.46
  			eXsection=5E-15  hXsection=5E-14
			##eJfactor=1.0 hJfactor=1.0
			)
  			(
  			name="state3" donor conc=@<fluence*0.9>@
  			Level FromValenceBand	EnergyMid=0.36
  			eXsection=2.5E-14  hXsection=2.5E-15
			##eJfactor=1.0 hJfactor=1.0
			)
		)
#if 1==1
  	HeavyIon("ion1")(
  		Direction=(0,1)
  		Location=(0.5,0)
  		Time=5e-9
  		###Length = [5e-4 8.0e-4 20.0e-4 22.0e-4]
  		##LET_f = [1e9 2e9 3e9 4e9]
  		##Wt_hi = [0.1e-4 0.2e-4 0.25e-4 0.1e-4]
  		Length = 100.0
  		LET_f = 0.3
  		Wt_hi = 0.5
  		Gaussian
  		PicoCoulomb
  		)
#endif

} 

CurrentPlot {
eLifeTime(Maximum(material="Silicon"))
hLifeTime(Maximum(material="Silicon"))
eAvalanche(Maximum(material="Silicon"))
hAvalanche(Maximum(material="Silicon"))
RadiationGeneration(Maximum(material="Silicon"))
HeavyIonGeneration(Maximum(material="Silicon"))

}

Math {

	Extrapolate

    Derivatives
  Avalderivatives
  Digits=7
  Notdamped=1000
  Iterations=15
  RelerrControl
  ErrRef(electron)=1e6
  ErrRef(hole)=1e6
  RhsMin=1e-15
  
  RecBoxIntegr
  
    eMobilityAveraging=ElementEdge       
  hMobilityAveraging=ElementEdge       
  ParallelToInterfaceInBoundaryLayer(-ExternalBoundary)

  Transient=BE
  
}


Solve {

##load(FilePrefix="n@node|REVERSE@")

#if 1==0
  Transient (  	initialtime=-1000 finaltime=0
   			MaxStep=100  MinStep=1e-15 InitialStep=1.0
			Increment=1.6 Decrement=4.0
			TurningPoints(		
			(  condition ( Time( -1e-9) )    value=1e-10 )
			(  condition ( Time( range=( -1e-9 0 )))   value=1e-10 )		

			)
                 )
                  { Coupled {  Poisson Electron Hole } } 
#endif

Poisson
  plugin { Poisson Electron Hole }
  Coupled { Poisson Electron Hole }

   Quasistationary (  		DoZero
   			MaxStep=0.02  MinStep=1e-8 InitialStep=1e-5
			Increment=1.6 Decrement=4.0
                  		Goal { Name="anode" Voltage=@<-HV>@ } 
                  	##BreakCriteria {Current (Contact = "anode" minval = -1e-9 )}
                 		)
                  { Coupled {  Poisson Electron Hole } } 
                  
 NewCurrent="HEAVY_"    
              	
   Transient (  	
   			##initialtime=-1000 finaltime=100.0e-9
   			initialtime=0 finaltime=100.0e-9

    			###MaxStep=1e-8
  			MaxStep=10 
   			MinStep=1e-18 InitialStep=1e-10
			Increment=1.6 Decrement=4.0
			TurningPoints(
			(  condition ( Time( range=( 0 @<5e-9 - 5e-11>@ )))   value=1e-10 )		
			(  condition ( Time( @<5e-9 - 5e-11>@) )    value=1e-12 )
			(  condition ( Time( range=( @<5e-9 - 5e-11>@ @<5e-9 + 5e-11>@ )))   value=1e-12 )		
			)
                 )
                  { 
                  	 Coupled {  Poisson Electron Hole } 
                  Plot(FilePrefix="n@node@_atmax_" time=(5e-9) nooverwrite )
                  } 
      


}
   

Plot {
	
        Current/Vector	
	eCurrent/Vector
	hCurrent/Vector
	eDensity
	hDensity
	ElectricField/Vector
	Potential
	CurrentPotential
  	DopingConcentration	
	eMobility
	hMobility
	DonorConcentration
	AcceptorConcentration
 	AvalancheGeneration
 	
 	eAvalanche hAvalanche
 	eLifeTime hLifeTime
 	
 	RadiationGeneration
 	HeavyIonGeneration


}


